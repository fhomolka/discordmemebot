import json
import discord
import random
import datetime
import os

random.seed(datetime.datetime.now())

#Open the token file
with open('token.json') as token_file:
    TOKEN = json.load(token_file)
    token_file.close()
#Set the token variable to the actual token, not to the entire dict
TOKEN = TOKEN['token']

#Connect to discord
client = discord.Client()

def is_me(message):
    return message.author == client.user

def is_command(message):
    return message.content.startswith('!')

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    #turn the message into lowercase, makes it easier on ifs
    message.content = message.content.lower() #.replace(' ', '') #might need this later

    if message.content.startswith('!'):

        #Delete previous 100 messages
        if message.content.startswith('!delet'):
            deleted = await client.purge_from(message.channel, limit=100, check=is_me)
            deleted += await client.purge_from(message.channel, limit=100, check=is_command)
            msg ='Deleted {} message(s)'.format(len(deleted))
            await client.send_message(message.channel, msg)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")

        #Bot Welcomes the user to uganda
        if message.content.startswith('!uganda'):
            msg = '{0.author.mention} https://www.youtube.com/watch?v=gu10vUyYj-g'.format(message)
            await client.send_message(message.channel, msg)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")

        #Bot links to 'Who Killed Captain Alex'
        if message.content.startswith('!alex'):
            msg = '{0.author.mention} https://www.youtube.com/watch?v=KEoGrbKAyKE'.format(message)
            await client.send_message(message.channel, msg)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")

        
        #Bot Answers who created it
        if message.content.startswith('!creator'):
            with open('responses/creator.txt', 'r') as response:  
                ResponseList = [current_word.rstrip() for current_word in response.readlines()]
                response.close()
            msg = ResponseList[random.randint(0, (len(ResponseList))-1)].format(message)
            await client.send_message(message.channel, msg)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")

        #Print the help messsage
        if message.content.startswith('!help'):
            embed=discord.Embed(title="Commands", description="List of commands", color=0x033d77)
            embed.add_field(name="!help", value="displays this message", inline=True)
            embed.add_field(name="!delet", value="Deletes last 100 messages created by <@500749487552331801>", inline=True)
            embed.add_field(name="!uganda", value="Gives you a proper welcome", inline=True)
            embed.add_field(name="!alex", value="Watch \"Who Killed Captain Alex\"", inline=True)
            await client.send_message(message.channel, embed=embed)
            print(message.content + " from  {0.author.mention}".format(message))
            print("Responded with \"" + embed + "\"")
            print("")

    #If the bot is greeted, respond
    if message.content.startswith('hello') or message.content.startswith('hi'):
        with open('responses/hello.txt', 'r') as response:  
    	    ResponseList = [current_word.rstrip() for current_word in response.readlines()]
    	    response.close()
        msg = ResponseList[random.randint(0, (len(ResponseList))-1)].format(message)
        await client.send_message(message.channel, msg)
        print(message.content + " from  {0.author.mention}".format(message))
        print("Responded with \"" + msg + "\"")
        print("")

    #Loss
    if 'loss' in message.content:

        with open('triggers/loss.txt', 'r') as trigger:  
            TriggerList = [current_word.rstrip() for current_word in trigger.readlines()]
            trigger.close()

        if any(word in message.content for word in TriggerList):
            with open('responses/loss.txt', 'r') as response:  
                ResponseList = [current_word.rstrip() for current_word in response.readlines()]
                response.close()
            await client.send_file(message.channel, ResponseList[random.randint(0, (len(ResponseList))-1)])
            print(message.content + " from {0.author.mention}".format(message))
            print("Sent a Picture of Loss")
            print("")

    #Traps are not gay
    if 'trap' in message.content:


        if 'are traps gay' in message.content:
            msg = 'No, traps are not gay, {0.author.mention}'.format(message)
            await client.send_message(message.channel, msg)
            print(message.content + " from {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")
            return

        with open('triggers/trap.txt', 'r') as trigger:  
            TriggerList = [current_word.rstrip() for current_word in trigger.readlines()]
            trigger.close()

        if any(word in message.content for word in TriggerList):
            with open('responses/trap.txt', 'r') as response:  
                ResponseList = [current_word.rstrip() for current_word in response.readlines()]
                response.close()
            msg = ResponseList[random.randint(0, (len(ResponseList))-1)].format(message)
            await client.send_message(message.channel, msg)
            print(message.content + " from {0.author.mention}".format(message))
            print("Responded with \"" + msg + "\"")
            print("")

    


@client.event
async def on_ready():
    await client.change_presence(game=discord.Game(name="with Ochinchin"))
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

os.system('clear')

client.run(TOKEN)